//Событию можно назначить обработчик, то есть функцию, которая сработает, как только событие произошло.
//Благодаря обработчикам JavaScript может реагировать на действия пользователя.

let input = document.getElementById('input');
let div = document.getElementsByTagName('div')[0];

let focusMethod = function getFocus() {
    input.style.outlineColor = 'green';
    input.style.outlineWidth = '1px';
};

input.addEventListener("blur", getBlur);

function getBlur() {
    if(+input.value < 0 || input.value === ''){
        input.style.outlineColor = 'red';
        input.style.outlineWidth = '1px';
        input.style.outlineStyle = 'solid';
        let p = document.createElement('p');
        p.innerHTML ='Please enter correct price';
        div.after(p);
    }else {
        let span = document.createElement('span');
        input.style.outlineWidth = '0';
        span.style.width = '30px';
        span.style.border = '1px solid grey';
        span.style.borderRadius = '30%';
        span.style.padding = '0 10px';
        span.style.margin = '0 3px';
        span.innerHTML = `Текущая цена: ${input.value}`;
        div.before(span);

        let btn = document.createElement('button');
        btn.style.height = '15px';
        btn.style.width = '15px';
        btn.style.padding = '0';
        btn.style.margin = '0 3px';
        btn.style.backgroundColor = '#fff';
        btn.innerHTML = 'X';
        btn.style.color = 'grey';
        btn.style.border = '1px solid grey';
        btn.style.borderRadius = '50%';
        span.append(btn);
        btn.addEventListener("click", toDeleteSpan);

        function toDeleteSpan() {
            span.remove();
            input.value = '';
        }
    }
}